// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebase : {
    apiKey: "AIzaSyCpdeHjSPY8LSJ8bYWrfmpYSd3rwgM4xcM",
    authDomain: "timejob-2d622.firebaseapp.com",
    databaseURL: "https://timejob-2d622.firebaseio.com",
    projectId: "timejob-2d622",
    storageBucket: "timejob-2d622.appspot.com",
    messagingSenderId: "1006263368049",
    appId: "1:1006263368049:web:c302f367d8c7ba612cded0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zon