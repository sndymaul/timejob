import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
jsondata:any;

  constructor(
  private navCtrl: NavController
  ) {
  this.initialjsondata();
  }

  initialjsondata() {
  this.jsondata=[
  	{
  		"title" : "spesial smabel",
  		"lokasi" : "sewon",
  		"img" : "assets/icon/LOGO.png"
  	},
  	{
  		"title" : "spesial smabel",
  		"lokasi" : "sewon",
  		"img" : "assets/icon/LOGO.png"
  	},
  	{
  		"title" : "spesial smabel",
  		"lokasi" : "sewon",
  		"img" : "assets/icon/LOGO.png"
  	},
  	{
  		"title" : "spesial smabel",
  		"lokasi" : "sewon",
  		"img" : "assets/icon/LOGO.png"
  	},
  	{
  		"title" : "spesial smabel",
  		"lokasi" : "sewon",
  		"img" : "assets/icon/LOGO.png"
  	}
  ];
  }

}
