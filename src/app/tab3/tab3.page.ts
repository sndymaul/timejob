import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthenticateService } from '../services/authentication.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(
  private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder
    ) {}
logout(){
	this.authService.logoutUser()
	.then(res =>{
	console.log(res);
	this.navCtrl.navigateBack('');
	})
	.catch(error => {
	console.log(error);
	})
}
}
